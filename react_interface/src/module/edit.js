import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import axios from 'axios';

const baseUrl = "http://18.191.109.212:8080"


class EditComponent extends React.Component{
 
 
  constructor(props){
    super(props);
    this.state = {
      dataFood:{},
      campName: "",
      campQuantity:"",
      campExpiration:"",
      stringCategory:"",
      selectCategory: 0
    }
  }
  
  componentDidMount(){
    let userId = this.props.match.params.id;
    const url = baseUrl+"/food/get/"+userId
    axios.get(url)
    .then(res=>{
      if (res.data.success) {
        const data = res.data.data[0]
        this.setState({
          dataFood:data,
          campName: data.name,
          campQuantity:data.quantity,
          campExpiration:data.expiration,
          stringCategory:data.category.category,
          selectCategory:data.categoryId
        })
        console.log(JSON.stringify(data.category.category))
      }
      else {
        alert("Error web service")
      }
    })
    .catch(error=>{
      alert("Error server "+error)
    })
  }
 
 
 render(){
  return (
     <div>
     
     
      <div class="form-row justify-content-center">
         <div class="form-group col-md-6">
          <label for="inputPassword4">Name</label>
          <input type="text" class="form-control"  placeholder="Name"
             value={this.state.campName} onChange={(value)=> this.setState({campName:value.target.value})}/>
         </div>
        
      </div>
      
      
      <div class="form-row">
         <div class="form-group col-md-6">
          <label for="inputState">Category</label>
          <select id="inputState" class="form-control" onChange={(value)=> this.setState({selectCategory:value.target.value})}>
             <option selected value={this.state.dataFood.categoryId}>{this.state.stringCategory}</option>
             <option value="1">Fruits & Vegetables</option>
             <option value="2">Meats</option>
             <option value="3">Dairy Products</option>
          </select>
         </div>
         
         
         <div class="form-group col-md-6">
          <label for="inputEmail4">Quantity</label>
          <input type="number" class="form-control"  placeholder="Quantity"
             value={this.state.campQuantity} onChange={(value)=> this.setState({campQuantity:value.target.value})}/>
         </div>
         
         
      </div>
      <div class="form-group col-md-6">
          <label for="inputEmail4">Expiration</label>
          <input type="number" class="form-control"  placeholder="Expiration"
             value={this.state.campExpiration} onChange={(value)=> this.setState({campExpiration:value.target.value})}/>
         </div>
      <button type="submit" class="btn btn-primary" onClick={()=>this.sendUpdate()}>Update</button>

     </div>
  );
 }
 
 
 sendUpdate(){
    //  get parameter id
    let userId = this.props.match.params.id;
    // url de backend
    const baseUrl = "http://18.191.109.212:8080/food/update/"+userId
    // parametros de datos post
    const datapost = {
      name : this.state.campName,
      quantity : this.state.quantity,
      expiration : this.state.expiration,
      category  : this.state.selectCategory
    }

    axios.post(baseUrl,datapost)
    .then(response=>{
      if (response.data.success===true) {
        alert(response.data.message)
      }
      else {
        alert("Error")
      }
    }).catch(error=>{
      alert("Error 34 "+error)
    })

   }
 
}


export default EditComponent;