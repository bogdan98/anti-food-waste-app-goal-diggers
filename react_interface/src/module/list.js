import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import axios from 'axios';

import { Link } from "react-router-dom";
import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/src/sweetalert2.scss'


// eslint-disable-next-line
const baseUrl = "http://18.191.109.212:8080"


class listComponent extends React.Component  {
    
    
    /////
    
      constructor(props){
      super(props);
      this.state = {
        listFood:[]
      }
    }
    
    componentDidMount(){
        
       this.loadFood()

    }
    
    loadFood() {
       const url = "http://18.191.109.212:8080/food/list"

      axios.get(url)
      .then(res => {
        if(res.data.success) {
            const data = res.data.data
            this.setState({listFood : data})
        }
        else
        {
            alert("Error web service")
        }
      })
      .catch(error => {
        alert("Error server" + error)
      })
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  render()
  {
    return (
      <table class="table table-hover table-striped">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Category</th>
            <th scope="col">Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Expiration</th>
            <th colspan="3">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th></th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
            <td>
              <button class="btn btn-outline-info "> Edit </button>
            </td>
            <td>
              <button class="btn btn-outline-danger "> Delete </button>
            </td>
          </tr>
          {this.loadFillData()}
        </tbody>
      </table>
    );
  }
  
  loadFillData(){

    return this.state.listFood.map((data)=>{
      return(
        <tr>
          <th>{data.id}</th>
          <td>{data.category.category}</td>
          <td>{data.name}</td>
          <td>{data.quantity}</td>
          <td>{data.expiration}</td>
          <td>
           
            
           
            
            <Link class="btn btn-outline-info "  to={"/edit/"+data.id}>Edit</Link>
            
            
          </td>
          <td>
           
            <button class="btn btn-outline-danger" onClick={()=>this.onDelete(data.id)}> Delete </button>

          </td>
        </tr>
      )
    })
  }
  
 onDelete(userId){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.sendDelete(userId)
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
  }
  
   sendDelete(userId)
  {
    // url de backend
    const baseUrl = "http://18.191.109.212:8080/food/delete"    // parameter data post
    // network
    axios.post(baseUrl,{
      id:userId
    })
    .then(response =>{
      if (response.data.success) {
        Swal.fire(
          'Deleted!',
          'Your food has been deleted.',
          'success'
        )
        this.loadFood()
      }
    })
    .catch ( error => {
      alert("Error 325 ")
    })
  }
}

export default listComponent;