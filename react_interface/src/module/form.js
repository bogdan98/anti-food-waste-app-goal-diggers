import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import axios from 'axios';

class EditComponent extends React.Component{
 
   constructor(props){
    super(props);
    this.state = {
      campName: "",
      campQuantity:"",
      campExpiration:"",
      selectCategory:0
    }
  }
 
  
   render(){
   return (
     <div>
       <div class="form-row justify-content-center">
         <div class="form-group col-md-6">
           <label for="inputPassword4">Name </label>
           <input type="text" class="form-control"  placeholder="Name" value={this.state.campName} onChange={(value)=> this.setState({campName:value.target.value})}/>
         </div>
         
         
       
       </div>
       
       <div class="form-row">
       
         <div class="form-group col-md-6">
           <label for="inputState">Category</label>
           <select id="inputState" class="form-control" onChange={(value)=> this.setState({selectCategory:value.target.value})}>
             <option selected>Choose...</option>
             <option value="1">Fruits & Vegetables</option>
             <option value="2">Meats</option>
             <option value="3">Dairy Products</option>
           </select>
         </div>
         
         <div class="form-group col-md-6">
           <label for="inputEmail4">Quantity</label>
           <input type="number" class="form-control"  placeholder="Quantity"  value={this.state.campQuantity} onChange={(value)=> this.setState({campQuantity:value.target.value})}/>
         </div>
       </div>
       
       <div class="form-group col-md-6">
           <label for="inputEmail4">Expiration</label>
           <input type="number" class="form-control"  placeholder="Expiration"  value={this.state.campExpiration} onChange={(value)=> this.setState({campExpiration:value.target.value})}/>
         </div>
       
       <button type="submit" class="btn btn-primary" onClick={()=>this.sendSave()}>Save</button>
     </div>
   );
 }
 
  sendSave(){
   
   // For selecting the category
   // eslint-disable-next-line
   if (this.state.selectCategory==0) {
     alert("Select Category Type")
   }
   // eslint-disable-next-line
   else if (this.state.campQuantity=="") {
      alert("select Quantity")
   }
   // eslint-disable-next-line
   else if (this.state.campName=="") {
      alert("Select Food Name")
   }
   // eslint-disable-next-line
   else if (this.state.campExpiration=="") {
      alert("Select Day Until Expiration")
   }
   // eslint-disable-next-line
   else {
    
    // URL backend
     // eslint-disable-next-line
     const baseUrl = "http://18.191.109.212:8080/food/create"


    // Parameters for post

     const datapost = {
       name : this.state.campName,
       quantity : this.state.campQuantity,
       expiration : this.state.campExpiration,
       category  : this.state.selectCategory
     }

     axios.post(baseUrl,datapost)
     .then(response=>{
       if (response.data.success===true) {
         alert(response.data.message)
       }
       else {
         alert(response.data.message)
       }
     }).catch(error=>{
       alert("Error 34 "+error)
     })

   }

 }
 
 
 
}


export default EditComponent;