//import sequelize
var Sequelize = require('sequelize');
// importing connection database
var sequelize = require('./database');
// import model for FK roleID


var Category = require('./Category');
var nametable = 'foods';

var Food = sequelize.define(nametable, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: Sequelize.STRING,
  quantity: Sequelize.BIGINT,
  expiration: Sequelize.BIGINT,
  categoryId: {
    type: Sequelize.INTEGER,
    // This is a reference to another model
    references: {
      model: Category,
      key: 'id'
    }
  }
},
{
	 timestamps: false,
});

Food.belongsTo(Category)

module.exports = Food