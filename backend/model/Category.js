var Sequelize = require('sequelize');
var sequelize = require('./database');

var nametable = 'category' // numele tabelei

var Category = sequelize.define(nametable, {
  category: Sequelize.STRING
},
{
	 timestamps: false,
});

module.exports = Category