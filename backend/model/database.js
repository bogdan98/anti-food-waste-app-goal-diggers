var Sequelize = require('sequelize');

const sequelize = new Sequelize(
  'fridge_db', // nume baza de date /// must be created MANUALLY
  'goaldiggers', //nume user // MYSQL USER should also be created manually and granted permissions
  'goaldiggers', //nume parola
  {
    host: 'localhost',
    dialect: 'mysql'
  }
);

module.exports = sequelize;