const controllers = {}

// Import model and sequelize

var sequelize = require('../model/database');
var Food = require('../model/Food');
var Category = require('../model/Category');

sequelize.sync();

controllers.delete = async (req, res) => {
  // parameter post
  const { id } = req.body;
  // delete sequelize
  const del = await Food.destroy({
    where: { id: id}
  })
  res.json({success:true,deleted:del,message:"Delete operation is successful"});
}


controllers.update = async (req,res) => {
  // parameter get id
  const { id } = req.params;
  // parameter POST
  const {name, quantity, expiration, category } = req.body;
  // Update data
  const data = await Food.update({
    name:name,
    quantity:quantity,
    expiration:expiration,
    categoryId:category
  },
  {
    where: { id: id}
  })
  .then( function(data){
    return data;
  })
  .catch(error => {
    return error;
  }) 
  res.json({success:true, data:data, message:"Update operation is successful"});
}






controllers.get = async (req,res) => {
  const { id } = req.params;
  const data = await Food.findAll({
      where: { id: id },
      include: [ Category ]
  })
  .then(function(data){
    return data;
  })
  .catch(error =>{
    return error;
  })
  res.json({ success: true, data: data });
}


controllers.list = async (req, res) => {

  const data = await Food.findAll({
    include: [ Category ]
  })
  .then(function(data){
    return data;
  })
  .catch(error => {
    return error;
  }); 

  res.json({success : true, data : data});

}

controllers.create = async (req,res) => {
  // data
  const { name, quantity, expiration, category } = req.body;
  console.log("Category is: " + category)
  // create
  const data = await Food.create({
    name: name,
    quantity: quantity,
    expiration: expiration,
    categoryId: category
  })
  .then(function(data){
    return data;
  })
  .catch(error =>{
    console.log("Error "+error)
    return error;
  })
  // return res
  res.status(200).json({
    success: true,
    message:"Success!",
    data: data
  });
}

// controllers.testdata = async (req,res) => {

//   const response = await sequelize.sync().then(function() {
      
//     //Create role
    Category.create({
     category:  'Fruits & Vegetables'
 });
 Category.create({
     category:  'Meats'
 });
 Category.create({
     category:  'Dairy Products'
 });


module.exports = controllers;