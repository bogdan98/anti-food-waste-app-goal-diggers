const express = require('express');
const router = express.Router();


const FoodController = require('../controllers/FoodController');

router.get('/list', FoodController.list);
router.post('/create',FoodController.create);
router.get('/get/:id',FoodController.get);
router.post('/update/:id', FoodController.update);
router.post('/delete',FoodController.delete);


module.exports = router;