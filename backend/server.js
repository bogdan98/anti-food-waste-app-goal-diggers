/// Importing express

const express = require('express');
const app = express();

/// Setting the port
app.set('port', process.env.PORT || 8080); // Server starts on port 8080

/// Middleware
app.use(express.json());
// Cors configuration
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

/// Importing route

const foodRouters = require('./routes/FoodRoute')
//Route
app.use('/food',foodRouters);

app.use('/test', (req, res) => {
  res.send("Route for testing");
});

app.use('/',(req,res)=>{
  res.send("This is Goaldiggers app server");
});

app.listen(app.get('port'),()=>{
  console.log("Start server on port "+app.get('port'))
})